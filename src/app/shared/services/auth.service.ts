import {Injectable} from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {Observable, of} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import firebase from 'firebase';
import {map, switchMap, take, tap} from 'rxjs/operators';
import {FbAuthResponse, User} from '../interfaces';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';


@Injectable()
export class AuthService {

  user$: Observable<firebase.User>

  constructor(private auth: AngularFireAuth,
              private afs: AngularFirestore,
              private router: ActivatedRoute) {
    this.user$ = auth.authState;
  }

  async gLogin() {

    let returnUrl = this.router.snapshot.queryParamMap.get("returnUrl") || '/';
    this.clearStorage();
    localStorage.setItem("returnUrl", returnUrl);

    const provider = new firebase.auth.GoogleAuthProvider();
    const credential = await this.auth.signInWithPopup(provider);
    return this.updateUserDate(credential.user);
  }

  gLogOut() {
    this.auth.signOut();
    this.clearStorage();
  }

  private updateUserDate(user: User) {
    const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${user.uid}`)

    const uData = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL
    }

    return userRef.set(uData, {merge: true});
  }

  private clearStorage() {
    if(localStorage.length) {
      localStorage.clear();
    }
  }

}
