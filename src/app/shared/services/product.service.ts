import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';

import {Product} from '../interfaces';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  save(product: Product): Observable<Product> {
    return this.http.post<Product>(`${environment.firebase.databaseURL}/products.json`, product);
  }

  getAll(): Observable<Product[]> {
    return this.http.get<Product[]>(`${environment.firebase.databaseURL}/products.json`)
                    .pipe(map(result => {
                        return Object.keys(result)
                                     .map(key => ({
                                       ...result[key],
                                       id: key
                                     }))
                      })
                    )
  }

  getById(id: string): Observable<Product> {
    return this.http.get<Product>(`${environment.firebase.databaseURL}/products/${id}.json`)
                    .pipe(
                      map(res => {
                        return {
                          ...res,
                          id,
                        }
                      })
                    );
  }

  update(product: Product): Observable<Product> {
    return this.http.patch<Product>(`${environment.firebase.databaseURL}/products/${product.id}.json`, product);
  }

  delete(id: string): Observable<void> {
    return this.http.delete<void>(`${environment.firebase.databaseURL}/products/${id}.json`);
  }
}
