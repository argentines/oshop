import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { DataTablesModule } from 'angular-datatables';

@NgModule({
  imports: [HttpClientModule,
            DataTablesModule],

  exports: [HttpClientModule, DataTablesModule]

})
export class SharedModule {}
