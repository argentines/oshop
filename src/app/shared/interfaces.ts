export interface User {
  uid: string;
  email: string;
  photoURL?: string;
  displayName?: string;
  isAdmin?: boolean;
}

export interface Admin {
  email: string;
  password: string;
  returnSecureToken?: boolean;
}

export interface FbAuthResponse {
  idToken: string;
  expiresIn: string;
  localId?: string;
}

export interface FbCreateResponse {
  name: string;
}

export interface Category {
  key: string;
  name: string;
}

export interface Product {
  id?: string,
  title: string,
  price: number,
  category: string,
  image: string
}
