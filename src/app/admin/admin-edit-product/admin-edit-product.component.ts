import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {Subscription} from 'rxjs';

import {ProductService} from '../../shared/services/product.service';
import {Category, Product} from '../../shared/interfaces';
import {CategoryService} from '../shared/services/category.service';

@Component({
  selector: 'admin-edit-product',
  templateUrl: './admin-edit-product.component.html',
  styleUrls: ['./admin-edit-product.component.scss']
})
export class AdminEditProductComponent implements OnInit {
  form: FormGroup;
  product: Product;
  submitted = false;
  pSub: Subscription;
  categories$: Category[];

  constructor(private productService: ProductService,
              private router: ActivatedRoute,
              private route: Router,
              private categoryService: CategoryService) { }

  ngOnInit(): void {

    this.categoryService.getCategories().subscribe(cat => this.categories$ = cat);

    this.router.params.pipe(
        switchMap((params: Params) => {
          return this.productService.getById(params['id']);
        })
    ).subscribe((res: Product) => {
      this.product = res; console.log('ID', res);
      this.form = new FormGroup({
        title: new FormControl(res.title, Validators.required),
        price: new FormControl(res.price, [Validators.required]),
        category: new FormControl(res.category, Validators.required),
        image: new FormControl(res.image, [Validators.required])
      })
    })
  }

  ngOnDestroy() {
    if(this.pSub) {
      this.pSub.unsubscribe();
    }
  }

  updateProduct() {

    if (this.form.invalid) {
      return;
    }

    this.submitted = false;
    this.pSub = this.productService.update({
      ...this.product,
      title: this.form.value.title,
      price: this.form.value.price,
      category: this.form.value.category,
      image: this.form.value.image
    }).subscribe(() => {
      this.submitted = true;
    });
  }

  deleteProduct(id: string) {
    if(!confirm('Are you sure you want to delete this product?')) return;

    this.productService.delete(id).subscribe(() => {
      this.route.navigate(['/admin', 'products'])
    })
  }
}
