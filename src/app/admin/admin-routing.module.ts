import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminLayoutComponent} from './shared/components/admin-layout/admin-layout.component';
import {AdminOrdersComponent} from './admin-orders/admin-orders.component';
import {AdminProductsComponent} from './admin-products/admin-products.component';
import {AdminLoginComponent} from './admin-login/admin-login.component';
import {AdminAuthGuard} from './shared/services/admin-auth.guard';
import {AdminAddProductComponent} from './admin-add-product/admin-add-product.component';
import {AdminEditProductComponent} from './admin-edit-product/admin-edit-product.component';

const routes: Routes = [
  {path: '', component: AdminLayoutComponent, children: [
      {path: '', redirectTo: 'admin/login', pathMatch: 'full'},
      {path: 'login', component: AdminLoginComponent},
      {path: 'products', component: AdminProductsComponent, canActivate: [AdminAuthGuard]},
      {path: 'product/new', component: AdminAddProductComponent, canActivate: [AdminAuthGuard]},
      {path: 'product/:id/edit', component: AdminEditProductComponent, canActivate: [AdminAuthGuard]},
      {path: 'orders', component: AdminOrdersComponent, canActivate: [AdminAuthGuard]}
  ]}
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AdminRoutingModule {}
