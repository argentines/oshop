import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {CategoryService} from '../shared/services/category.service';
import {Product} from '../../shared/interfaces';
import {ProductService} from '../../shared/services/product.service';
import {Router} from '@angular/router';

@Component({
  selector: 'admin-add-product',
  templateUrl: './admin-add-product.component.html',
  styleUrls: ['./admin-add-product.component.scss']
})
export class AdminAddProductComponent implements OnInit {

  form: FormGroup;
  categories$;

  constructor(private categoryService: CategoryService,
              private productService: ProductService,
              private router: Router) { }

  ngOnInit(): void {
    this.categoryService.getCategories().subscribe(cat => this.categories$ = cat);

    this.form = new FormGroup({
      title: new FormControl(null, Validators.required),
      price: new FormControl(null, [Validators.required, Validators.min(0)]),
      category: new FormControl(null, Validators.required),
      image: new FormControl(null, [Validators.required])
    })
  }

  submit() {
    if (this.form.invalid) {
      return;
    }

    const product: Product = {
      title: this.form.value.title,
      price: this.form.value.price,
      category: this.form.value.category,
      image: this.form.value.image
    }

    this.productService.save(product)
      .subscribe(() => {
        this.form.reset();
        this.router.navigate(['/admin', 'products']);
      })

  }
}
