import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AdminAuthService} from './admin-auth.service';
import {map, tap} from 'rxjs/operators';

@Injectable()
export class AdminAuthGuard implements CanActivate{

  constructor(private auth: AdminAuthService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if(this.auth.isAuthenticated()) {
      return  this.auth.userRole(localStorage.getItem('fb-userId'))
        .pipe(
          tap(user => user),
          map( user => {
            this.auth.isAdmin = user.isAdmin;
            return true;
          })
        )
    }else {
      this.auth.aLogout();
      this.router.navigate(['/admin', 'login'], {
        queryParams: {
          loginAgain: true
        }
      });
    }
  }
}
