import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Admin, FbAuthResponse, User} from '../../../shared/interfaces';
import {Observable} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';


@Injectable()
export class AdminAuthService {

  isAdmin: boolean;

  get token(): string {
    const expDate = new Date(localStorage.getItem('fb-expDate'));
    if(new Date() > expDate) {
      this.aLogout();
      return null;
    }
    return localStorage.getItem('fb-idToken');
  }

  constructor(private http: HttpClient) {}

  aLogin(user: Admin): Observable<any> {

    if(localStorage.length)
      localStorage.clear();

    user.returnSecureToken = true;
    return this.http.post(environment.user.fauth + environment.firebase.apiKey, user)
                    .pipe(
                      tap(this.setToken),
                      catchError(this.handleError.bind(this))
                    );
  }

  aLogout() {
    this.setToken(null);
  }

  userRole(uid: string): Observable<any> {
    return this.http.get(`${environment.firebase.databaseURL}/users/${uid}.json`);
  }

  isAuthenticated(): boolean {
    return !!this.token;
  }

  private handleError(error: HttpErrorResponse) {
    console.log(error);
  }

  private setToken(user: FbAuthResponse) {
    if(user) {
      const expDate = new Date(new Date().getTime() + +user.expiresIn * 1000);
      localStorage.setItem('fb-idToken', user.idToken);
      localStorage.setItem('fb-userId', user.localId);
      localStorage.setItem('fb-expDate', expDate.toString());
    }else {
      localStorage.clear();
    }
  }
}
