import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {AdminAuthService} from './admin-auth.service';
import {catchError} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable()
export class AdminAuthInterceptor implements HttpInterceptor{

  constructor(private auth: AdminAuthService, private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(this.auth.isAuthenticated()) {
      req = req.clone({
        setParams: {
          auth: this.auth.token
        }
      })
    }
    return next.handle(req)
               .pipe(
                 catchError((error) => {
                   console.log('[Interceptor Error]', error);

                   if(error == 401) {
                     this.auth.aLogout();
                     this.router.navigate(['/admin', 'login'], {
                       queryParams: {
                         authFailed: true
                       }
                     });
                   }

                   return throwError(error);
                 })
               )
  }

}
