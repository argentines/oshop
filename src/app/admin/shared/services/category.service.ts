import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {Category} from '../../../shared/interfaces';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

  getCategories(): Observable<Category[]> {
    return this.http.get(`${environment.firebase.databaseURL}/categories.json`)
                    .pipe(
                      map((cat) => {
                        return Object.keys(cat)
                                   .map(key => ({
                                     key: key,
                                     name: cat[key].name
                                   }))
                      })
                    )
  }
}
