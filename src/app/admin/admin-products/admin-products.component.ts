import { Component, OnInit } from '@angular/core';
import {ProductService} from '../../shared/services/product.service';
import {Product} from '../../shared/interfaces';
import {Subject, Subscription} from 'rxjs';

@Component({
  selector: 'admin-products',
  templateUrl: './admin-products.component.html',
  styleUrls: ['./admin-products.component.scss']
})
export class AdminProductsComponent implements OnInit {

  products: Product[];
  pSub: Subscription;
  allSub: Subscription;
  productName;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.allSub = this.productService.getAll()
      .subscribe(products => {
                this.products = products;
                this.dtTrigger.next();
      });

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 4,
      responsive: true,
      searching: false,
      dom: '<"top"<"clear">>rt<"bottom"i flp<"clear">>',
      columnDefs: [
        {
          targets: ['_all'],
          className: ''
        }
      ]
    };
  }

  ngOnDestroy() {
    if(this.pSub) {
      this.pSub.unsubscribe();
    }

    if(this.allSub) {
      this.allSub.unsubscribe();
    }

    if(this.dtTrigger) {
      this.dtTrigger.unsubscribe();
    }
  }
}
