import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Admin} from '../../shared/interfaces';
import {AdminAuthService} from '../shared/services/admin-auth.service';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.scss']
})
export class AdminLoginComponent implements OnInit, OnDestroy {
  form: FormGroup;
  message: string = '';
  submitted: boolean;
  aSub: Subscription;

  constructor(private auth: AdminAuthService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required])
    })

  }

  ngOnDestroy(): void {
    if(this.aSub) {
      this.aSub.unsubscribe();
    }
  }

  submit() {

    if(this.form.invalid) {
      return;
    }

    this.submitted = true;

    const admin: Admin = {
      email: this.form.value.email,
      password: this.form.value.password
    }
    this.submitted = false;

    this.aSub = this.auth.aLogin(admin).subscribe((user) => { console.log(user);
      this.form.reset();
      this.router.navigate(['/admin', 'products']);
      this.submitted = false;
    }, () => this.submitted = false)
  }


}
