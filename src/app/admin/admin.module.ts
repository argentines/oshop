import {NgModule, Provider} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AdminRoutingModule} from './admin-routing.module';
import {AdminLayoutComponent} from './shared/components/admin-layout/admin-layout.component';
import {AdminOrdersComponent} from './admin-orders/admin-orders.component';
import {AdminProductsComponent} from './admin-products/admin-products.component';
import {AdminAuthGuard} from './shared/services/admin-auth.guard';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AdminAuthService} from './shared/services/admin-auth.service';
import {SharedModule} from '../shared/shared.module';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {AdminAuthInterceptor} from './shared/services/admin-auth.interceptor';
import { AdminNavbarComponent } from './admin-navbar/admin-navbar.component';
import { AdminAddProductComponent } from './admin-add-product/admin-add-product.component';
import { AdminEditProductComponent } from './admin-edit-product/admin-edit-product.component';
import {AuthGuard} from '../shared/services/auth.guard';
import {SearchPipe} from '../shared/services/search.pipe';

const INTERCEPTOR_PROVIDER: Provider = {
  provide: HTTP_INTERCEPTORS,
  multi: true,
  useClass: AdminAuthInterceptor
}

@NgModule({
  declarations: [AdminLayoutComponent,
                 AdminOrdersComponent,
                 AdminProductsComponent,
                 AdminLoginComponent,
                 AdminNavbarComponent,
                 AdminAddProductComponent,
                 AdminEditProductComponent,
                 SearchPipe],
  imports: [AdminRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    FormsModule
  ],
  exports: [],
  providers: [AdminAuthGuard,
              AdminAuthService,
              INTERCEPTOR_PROVIDER]
})

export class AdminModule {}
