import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './view/home/home.component';
import {MainLayoutComponent} from './shared/components/main-layout/main-layout.component';
import {ProductsComponent} from './view/products/products.component';
import {ShoppingCartComponent} from './view/shopping-cart/shopping-cart.component';
import {CheckOutComponent} from './view/check-out/check-out.component';
import {OrderSuccessComponent} from './view/order-success/order-success.component';
import {MyOrdersComponent} from './view/my-orders/my-orders.component';
import {LoginComponent} from './view/login/login.component';
import {AuthGuard} from './shared/services/auth.guard';

const routes: Routes = [
  {path: '', component: MainLayoutComponent, children: [
      /*{path: '', redirectTo: '/', pathMatch: 'full'},*/
      {path: '', component: ProductsComponent},
      {path: 'login', component: LoginComponent},
      /*{path: 'products', component: ProductsComponent},*/
      {path: 'shopping-cart', component: ShoppingCartComponent},
      {path: 'check-out', component: CheckOutComponent, canActivate: [AuthGuard]},
      {path: 'order-success', component: OrderSuccessComponent},
      {path: 'my/orders', component: MyOrdersComponent}
  ]},
  {
    path: 'admin', loadChildren: () => import("./admin/admin.module").then(m => m.AdminModule)
  }
]

@NgModule({
 imports: [RouterModule.forRoot(routes)],
 exports: [RouterModule]
})
export class AppRoutingModule {}
