import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../shared/services/auth.service';


@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private auth: AuthService) { }

  ngOnInit(): void {

  }

  login() {
    if(localStorage.length) {
      localStorage.clear();
    }
    this.auth.gLogin();
  }

  logout() {
    this.auth.gLogOut();
  }

}
