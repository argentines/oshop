import { Component, OnInit } from '@angular/core';
import {ProductService} from '../../shared/services/product.service';
import {Category, Product} from '../../shared/interfaces';
import {CategoryService} from '../../admin/shared/services/category.service';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Params} from '@angular/router';
import {switchMap} from 'rxjs/operators';

@Component({
  selector: 'products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  products: Product[];
  categories: Category[];
  filteredProduct: Product[] = [];
  category: string;
  pSub: Subscription;
  cSub: Subscription;

  constructor(private productService: ProductService,
              private categoryService: CategoryService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
   this.pSub = this.productService.getAll()
                    .pipe(switchMap(products => {
                                  this.products = products;
                                  return this.route.queryParamMap;
                      })).subscribe((params: Params) => {
                          this.category = params.get('category');
                          this.filteredProduct = (this.category) ?
                            this.products.filter(p => p.category === this.category) :
                            this.products;
                      });
    this.cSub = this.categoryService.getCategories()
      .subscribe(cat => this.categories = cat);
  }

  ngOnDestroy() {
    if(this.pSub) {
      this.pSub.unsubscribe();
    }

    if(this.cSub) {
      this.cSub.unsubscribe();
    }
  }

}
